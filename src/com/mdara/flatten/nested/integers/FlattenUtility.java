package com.mdara.flatten.nested.integers;

import java.util.LinkedList;
import java.util.List;

public class FlattenUtility {

		//return flat list
	public static List<Object> convertToFlat(List<?> list) {
		List<Object> flatList = new LinkedList<Object>();
		toFlat(list, flatList);
		return flatList;
	}
 
	//Recursive toFlat overriden method
	public static void toFlat(List<?> nestedList, List<Object> flatList) {
		for (Object item : nestedList) {
			if (item instanceof List<?>) {
				toFlat((List<?>) item, flatList);
			} else {
				flatList.add(item);
			}
		}
	}

}
