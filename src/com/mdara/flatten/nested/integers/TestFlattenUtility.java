package com.mdara.flatten.nested.integers;

import java.util.Arrays;
import java.util.List;

public class TestFlattenUtility {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		//Test 1
		List<Object> nestedList1 = list(list(6), 4, list(list(6, 3), 9), list(list(list())), list(list(list(2))), 5, 1, list());
		List<Object> flatList1 = FlattenUtility.convertToFlat(nestedList1);
		print("Test 1 Nested    : ", "Test 1 flattened : ", nestedList1, flatList1);
		
		//Test 2
		List<Object> nestedList2 = list(list(4,list(4,9)), list(list(list(2))));
		List<Object> flatList2 = FlattenUtility.convertToFlat(nestedList2);
		print("Test 1 Nested2    : ", "Test 1 flattened2 : ", nestedList2, flatList2);
		
		//Test 3
		List<Object> nestedList3 =  list(list(list(6, 3), 9),list(list(2,4)));
		List<Object> flatList3 = FlattenUtility.convertToFlat(nestedList3);
		print("Test 3 Nested    : ", "Test 3 flattened : ", nestedList3, flatList3);
		
		//Test 4
		List<Object> nestedList4 = list(9, list(list(list(2))), list(list(6, 3), 9), list(6), 5, 1, list());
		List<Object> flatList4 = FlattenUtility.convertToFlat(nestedList4);
		print("Test 4 Nested    : ", "Test 4 flattened : ", nestedList4, flatList4);

		//Test 5
		List<Object> nestedList5 = list(list(list(list(10, 13), 19), 3), 4, list(1,list(4,list(3,5),2)), list(list(list(2))), 6, 1, list(8,3));
		List<Object> flatList5 = FlattenUtility.convertToFlat(nestedList5);
		print("Test 5 Nested    : ", "Test 5 flattened : ", nestedList5, flatList5);
	} 
	
	//Convert data to nested list
	private static List<Object> list(Object... a) {
		return Arrays.asList(a);
	}
	
	
	//print  results
	public static void print(String testLabel, String resultLabel, List<Object> treeList, List<Object> flatList) {
		System.out.println(testLabel + treeList);
		System.out.println(resultLabel + flatList);
		System.out.println("\n\n");
	}
}
